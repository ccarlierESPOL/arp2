package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;

import java.util.Properties;

public class ConnectionDatabase {
	
	private static String driver = "com.mysql.cj.jdbc.Driver";
	private static String database = "focus_enterprise";
	private static String hostname = "localhost";
	
	// Path
	private static String url = "jdbc:mysql://" + hostname + "/" + database;
	
	// Connection
	private static Connection connectMySQL() {
		Connection connection = null;
		Properties properties = new Properties();
		properties.put("user", "userp");
		properties.put("password", "Userp@092");
		
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, properties);
		} catch(Exception e) {
			System.out.println("Error al conectar a la base de datos");
		}
		return connection;
	}
	
	// Validate user
	public static String validate_login(String data) {
		String result = null;
		String query = "SELECT * FROM usuario";
		
		String[] array = data.split(",");
		if(array.length != 2) return result;
		String usuario = array[0];
		String pass = array[1];
		
		Connection db = null;
		Statement statement = null;
		ResultSet resultset = null;
		
		try {
			db = connectMySQL();
			statement = db.createStatement();
			resultset = statement.executeQuery(query);
			while(resultset.next()) {
				if(usuario.equals(resultset.getString("usuario")) && 
						pass.equals(resultset.getString("contrasena"))) result = resultset.getString("nombre"); 
			}
		}catch(Exception e) {
			System.out.println("Problemas al validar login..." + e);
		}
		
		return result;
	}
}
