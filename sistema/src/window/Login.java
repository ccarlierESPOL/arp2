package window;

import database.ConnectionDatabase;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class Login extends Application{
	
	// root
	private AnchorPane root;
	private VBox vbox;
	
	// User Information
	private Label lbuser;
	private TextField user;
	private HBox huser;
	
	// Password Information
	private Label lbpass;
	private PasswordField pass;
	private HBox hpass;
	
	// Button
	private Button exit;
	private Button login;
	
	public Login() {
		root = new AnchorPane();
		vbox = new VBox();
		
		// Labels
		lbpass = new Label("Contrasena");
		lbuser = new Label("Usuario   ");
		
		// TextFields
		user = new TextField();
		pass = new PasswordField();
		
		// Buttons
		exit = new Button("Salir");
		AnchorPane.setBottomAnchor(exit, 10.0);
		AnchorPane.setRightAnchor(exit, 10.0);
		
		login = new Button("Iniciar Sesion");
		AnchorPane.setLeftAnchor(login, 55.0);
		AnchorPane.setTopAnchor(login, 130.0);
		
		// HBox
		huser = new HBox();
		hpass = new HBox();
		manageHBox();
	}
	
	@Override
	public void start(Stage stage) {
		exit.setOnAction(e -> stage.close());
		login.setOnAction(e -> {
			String data = user.getText() + "," + pass.getText();
			String value = ConnectionDatabase.validate_login(data);
			if(value != null) {
				Home home = new Home(value);
				home.start(stage);
			}
		});
		
		root.getChildren().addAll(exit, login);
		
		Scene scene = new Scene(root, 616.0, 426.0);
		stage.setScene(scene);
		stage.setTitle("Inicie Sesion para continuar");
		stage.centerOnScreen();
		stage.show();
	}
	
	private void manageHBox() {
		huser.getChildren().addAll(lbuser, user);
		huser.setSpacing(26.0);
		
		hpass.getChildren().addAll(lbpass, pass);
		hpass.setSpacing(15.0);
		
		vbox.getChildren().addAll(huser, hpass);
		vbox.setSpacing(15.0);
		
		AnchorPane.setLeftAnchor(vbox, 10.0);
		AnchorPane.setTopAnchor(vbox, 50.0);
		root.getChildren().add(vbox);
	}
}
