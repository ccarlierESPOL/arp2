package window;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.layout.AnchorPane;

import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.text.Font;

public class SplashScreen extends Application{
	
	// Root and HBox
	private AnchorPane root;
	
	// Button to go to principal window and Label
	private Button goPrincipalWindow;
	private Label nameCompany;
	
	// Constructor
	public SplashScreen() {
		// Initialize root
		root = new AnchorPane();
				
		// Other components
		goPrincipalWindow = new Button("Ingresar");
		
		nameCompany = new Label("FOCUS INC.");
		nameCompany.setFont(new Font("Ariel", 30));
	}
	
	@Override
	public void start(Stage stage) {
		// Logic of the button
		goPrincipalWindow.setOnAction(e -> {
			Login login = new Login();
			login.start(stage);
		});
		
		// Adding other components to the root
		AnchorPane.setLeftAnchor(nameCompany, 120.0);
		AnchorPane.setTopAnchor(nameCompany, 200.0);
		
		AnchorPane.setLeftAnchor(goPrincipalWindow, 400.0);
		AnchorPane.setTopAnchor(goPrincipalWindow, 205.0);
		
		root.getChildren().addAll(nameCompany, goPrincipalWindow);
		
		Scene scene = new Scene(root, 700.0, 400.0);
		stage.setScene(scene);
		stage.setTitle("Bienvenidos");
		stage.centerOnScreen();
		stage.setResizable(false);
		stage.show();
	}
	
	// Method to start everything
	public static void main(String[] args) {
		launch(args);
	}
}