package window;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

import javafx.scene.layout.AnchorPane;

import javafx.scene.control.Label;
import javafx.scene.control.Button;

public class Home extends Application{
	
	// root
	private AnchorPane root;
	
	// Label for name
	private Label name;
	
	// Buttons
	private Button exit;
	private Button logout;
	
	public Home(String nombre) {
		root = new AnchorPane();
		
		// Name of the user
		name = new Label("Bienvenido " + nombre);
		AnchorPane.setTopAnchor(name, 5.0);
		AnchorPane.setLeftAnchor(name, 5.0);
		
		// Buttons
		exit = new Button("Salir");
		AnchorPane.setRightAnchor(exit, 10.0);
		AnchorPane.setBottomAnchor(exit, 10.0);
		
		logout = new Button("Cerrar Sesion");
		AnchorPane.setLeftAnchor(logout, 10.0);
		AnchorPane.setBottomAnchor(logout, 10.0);
	}
	
	@Override
	public void start(Stage stage) {
		exit.setOnAction(e -> stage.close());
		logout.setOnAction(e -> {
			Login login = new Login();
			login.start(stage);
		});
		
		root.getChildren().addAll(name, exit, logout);
		
		Scene scene = new Scene(root, 800.0, 600.0);
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setTitle("Pagina Principal");
		stage.show();
	}
}
